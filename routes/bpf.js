var models = require('../models');
var express = require('express');
var http = require('http');
var router = express.Router();
var dateFormat = require('dateformat');


/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('bpf', {
        title: 'My account'
    });
});

router.post('/', function (req, res, next) {
    req.checkBody("agreement_number", "Enter a valid agreement number.").isLength(16, 16);
    req.checkBody("dob", "Enter a valid date of birth - 01/01/1901.").matches(/\d{2}\/\d{2}\/\d{4}/);
    req.checkBody("postcode", "Enter a valid postcode.").matches(
        /^[A-PR-UWYZ]([0-9]{1,2}|([A-HK-Y][0-9]([0-9ABEHMNPRV-Y])?)|[0-9][A-HJKPS-UW]) ?[0-9][ABD-HJLNP-UW-Z]{2}$/i
    );

    var errors = req.validationErrors();
    if (errors) {
        res.render('bpf', {errors: errors});
    } else {
        models.Customer.findOne({
            where: {
                agreement_number: parseInt(req.body.agreement_number),
                dob: req.body.dob,
                postcode: req.body.postcode
            }
        }).then(function (user) {
            if (user) {
                var now = new Date();
                res.render('bpf', {
                    title: 'My account',
                    details: {
                        name: user.first_name + " " + user.last_name,
                        balance: "£" + user.balance,
                        nextPaymentAmount: "£1.00",
                        nextPaymentDate: dateFormat(now, " dS mmmm, yyyy")
                    }
                });
            } else {
                req.flash('error', 'Sorry your details were not recognized');
                res.render('bpf', {
                    title: 'My account',
                    message: req.flash('error')
                });
            }
        });
    }
});

module.exports = router;