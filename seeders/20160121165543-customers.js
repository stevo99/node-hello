'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('Person', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
    return queryInterface.bulkInsert('Customers', [
      {
        first_name: 'John',
        last_name: 'Doe',
        balance: 1000.00,
        createdAt: 20160121165543,
        updatedAt: 20160121165543,
        agreement_number:1234567890123456,
        dob: 19760121165543,
        postcode: 'NN47SG'
      },
      {
        first_name: 'Jane',
        last_name: 'Smith',
        balance: 500.00,
        createdAt: 20160121165544,
        updatedAt: 20160121165544,
        agreement_number:2345678901234567,
        dob: 19760121165543,
        postcode: 'NN47SG'
      }
    ], {});
  },

  down: function (queryInterface, Sequelize) {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('Person', null, {});
    */
    return queryInterface.bulkDelete('Customers', null, {});
  }
};
