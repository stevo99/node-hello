'use strict';

module.exports = {
    up: function (queryInterface, Sequelize) {
        return [
            queryInterface.addColumn(
                'Customers',
                'agreement_number',
                {
                    type: Sequelize.STRING,
                    allowNull: false
                }
            ),
            queryInterface.addColumn(
                'Customers',
                'dob',
                {
                    type: Sequelize.DATE,
                    allowNull: false
                }
            ),
            queryInterface.addColumn(
                'Customers',
                'postcode',
                {
                    type: Sequelize.STRING,
                    allowNull: false
                }
            )
        ];
    },

    down: function (queryInterface, Sequelize) {
        /*
         Add reverting commands here.
         Return a promise to correctly handle asynchronicity.

         Example:
         return queryInterface.dropTable('users');
         */
    }
};
