class Login < SitePrism::Page

  set_url '/bpf'


  element :agreement_number, '#agreement_number'
  element :date_of_birth, '#date_of_birth'
  element :postcode, '#postcode'
  element :submit, '.button'
  element :error_message, 'p.error'
  element :validation_error, 'li.error_message'

  def login_user(user_details)
    agreement_number.set(user_details[:agreement_no])
    date_of_birth.set(user_details[:dob])
    postcode.set(user_details[:postcode])
    submit.click
  end

  def agreement_error_text
    'Enter a valid agreement number.'
  end

  def dob_error_text
    'Enter a valid date of birth - 01/01/1901.'
  end

  def postcode_error_text
    'Enter a valid postcode.'
  end


end