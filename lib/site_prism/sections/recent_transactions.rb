class RecentTransactions < SitePrism::Section

  element :name, 'td.name'
  element :balance, 'td.balance'
  element :next_payment, 'td.next-payment'
  element :payment_date, 'td.payment-date'

end