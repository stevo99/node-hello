require 'date'
require 'namey'

class Customer < DBConnection

  self.table_name = 'Customers'
  self.primary_key = :id

  def self.create_customer
    gen = Namey::Generator.new
    name_arr = gen.name(:rare).gsub(/\s+/m, ' ').strip.split(' ')

    create(first_name: name_arr.first,
           last_name: name_arr[1],
           balance: rand(600..10000),
           createdAt: Time.now.to_date,
           updatedAt: Time.now.to_date,
           agreement_number: (1..16).map{'123456789'.chars.to_a.sample}.join.to_i,
           dob: '01/01/1990',
           postcode: 'WD194RE'
    )
  end

end