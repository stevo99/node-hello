output_dir = 'screenshots/'

Before do
  @app = App.new
end

After do |scenario|
  if scenario.failed?
    page.driver.browser.save_screenshot("#{output_dir}#{scenario.__id__}.png")
    embed("#{output_dir}#{scenario.__id__}.png", "image/png", "Screenshot")
  end
end

After do
  if @customer
    Customer.delete(@customer.id)
  end
end