require 'rspec'
require 'rspec/expectations'
require 'capybara/dsl'
require 'capybara/cucumber'
require 'capybara/rspec'
require 'selenium-webdriver'
require 'pry'
require 'site_prism'
require 'require_all'
require 'yaml'
require 'rake'
require 'active_support/core_ext/string/inflections'
require 'active_record'
require 'mysql2'

require File.dirname(__FILE__) + '/../../lib/site_prism/app'
require_all File.dirname(__FILE__) + '/../../lib/site_prism/sections'
require_all File.dirname(__FILE__) + '/../../lib/site_prism/pages'
require_all File.dirname(__FILE__) + '/../../lib/db'
require_all File.dirname(__FILE__) + '/../../lib/models'

ENV['APP_URL'] ||= 'http://node-hello:8080'

Capybara.register_driver :selenium do |app|
  profile = Selenium::WebDriver::Firefox::Profile.new
  Capybara::Selenium::Driver.new(app, :profile => profile)
end

Capybara.default_driver = :selenium
Capybara.default_max_wait_time = 5
Capybara.app_host = ENV['APP_URL']

