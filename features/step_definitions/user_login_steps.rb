Given(/^I am on the login page$/) do
  @app.login.load
end

When(/^I login with (.*) credentials$/) do |login_type|
  case login_type
    when 'valid'
      p @customer = Customer.create_customer
      @app.login.login_user({agreement_no: @customer.agreement_number, dob: @customer.dob, postcode: @customer.postcode})
    when 'invalid'
      @app.login.login_user({agreement_no: '1234567890123456', dob: '01/01/1970', postcode: 'EC1A 1AA'})
    else
      raise 'expected either valid or invalid as options'
  end

end

When(/^I login with "([^"]*)" "([^"]*)" "([^"]*)"$/) do |agreement, dob, postcode|
  user_hash = {agreement_no: agreement, dob: dob, postcode: postcode}
  @app.login.login_user(user_hash)
end

Then(/^my account balance will be displayed$/) do
  results = @app.my_account.recent_transactions

  expect(results.name.text).to eq(@customer.first_name + ' ' + @customer.last_name)
  expect(results.balance.text).to eq('£' + @customer.balance.to_s)
end

Then(/^I will not be logged into my account$/) do
 expect(@app.login.wait_for_agreement_number).to be true
end

And(/^an incorrect details error will be displayed$/) do
  expect(@app.login.error_message.text).to eq('Sorry your details were not recognized')
end


And(/^an error will be displayed for the (.*) field$/) do |field_name|
  expect(@app.login.validation_error.text).to eq(@app.login.send(field_name + '_error_text'))
end