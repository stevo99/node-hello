Feature: User Login

  As a Barclays Partner Finance customer
  I want to be able to login to the site to view my transactions

  Scenario: Successful login
    Given I am on the login page
    When I login with valid credentials
    Then my account balance will be displayed

  Scenario: Unsuccessful login
    Given I am on the login page
    When I login with invalid credentials
    Then I will not be logged into my account
    And an incorrect details error will be displayed

  Scenario Outline: Login form validations - Agreement Number
    Given I am on the login page
    When I login with "<agreement>" "<dob>" "<postcode>"
    Then I will not be logged into my account
    And an error will be displayed for the agreement field
    Examples:
      | agreement           | dob        | postcode |
      |                     | 01/01/1950 | EC11AA   |
      | 123456789012345     | 01/01/1950 | EC11AA   |
      | 12345678901234567   | 01/01/1950 | EC11AA   |
      | 1234 5678 9012 3456 | 01/01/1950 | EC11AA   |

  Scenario Outline: Login form validations - Date of Birth
    Given I am on the login page
    When I login with "<agreement>" "<dob>" "<postcode>"
    Then I will not be logged into my account
    And an error will be displayed for the dob field
    Examples:
      | agreement        | dob        | postcode |
      | 1234567890123456 |            | EC11AA   |
      | 1234567890123456 | 01011950   | EC11AA   |
      | 1234567890123456 | 01/01/50   | EC11AA   |
      | 1234567890123456 | 01-01-1950 | EC11AA   |

  Scenario Outline: Login form validations - Postcode
    Given I am on the login page
    When I login with "<agreement>" "<dob>" "<postcode>"
    Then I will not be logged into my account
    And an error will be displayed for the postcode field
    Examples:
      | agreement        | dob        | postcode |
      | 1234567890123456 | 01/01/1950 |          |
      | 1234567890123456 | 01/01/1950 | EAA1AA   |
      | 1234567890123456 | 01/01/1950 | ECC11AA  |
      | 1234567890123456 | 01/01/1950 | EC111A   |
      | 1234567890123456 | 01/01/1950 | EC11A    |





